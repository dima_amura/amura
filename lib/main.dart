import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        dividerTheme: DividerThemeData( 
        space: 50,
              color: Colors.blue,
              thickness: 10.0,
              indent: 50,
              endIndent: 20,
      ),
      ),
      title: 'Flutter App',
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text('Flutter App')),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add),
          backgroundColor: Colors.blue,
          //elevation: 250.0,
          mini: true,
          tooltip: "Добавить транзакцию",
          onPressed: () => null,
        ),
        bottomNavigationBar: BottomAppBar(
            color: Colors.lime,
            child: Container(
              height: 50.0,
            )),
        floatingActionButtonLocation: FloatingActionButtonLocation.endDocked,
        body: Column(
          children: [
            Container(
              height: 100.0,
              width: double.infinity,
              child: Stack(
                  fit: StackFit.loose,
                  alignment: Alignment.center,
                  children: [
                    Positioned(
                      top: 20.0,
                     left: 75.0,
                      child: Text("Привет", style: TextStyle(fontSize: 25)),
                    ),
                    Text("как дела", style: TextStyle(fontSize: 25)),
                    Positioned(
                      bottom: 20.0,
                      right: 30.0,
                      child: Text(
                        'что делаешь',
                        style: TextStyle(fontSize: 25),
                      ),
                    )
                  ]),
            ),
            Divider(
             // height: 50,
             // color: Colors.blue,
             // thickness: 10.0,
             // indent: 50,
             // endIndent: 20,
            ),
            Text("Разделитель сверху"),
            Spacer(flex: 2,),
            Text("Spacer",style: TextStyle(fontSize: 25)),
            Spacer(flex: 2,),
            Text("Spacer2",style: TextStyle(fontSize: 25)),
            Spacer(flex: 1,),
            Text("Spacer3",style: TextStyle(fontSize: 25)),
          ],
        ));
  }
}
